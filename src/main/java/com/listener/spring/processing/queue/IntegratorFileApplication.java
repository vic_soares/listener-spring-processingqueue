package com.listener.spring.processing.queue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class IntegratorFileApplication {

    public static void main(String[] args) {
        SpringApplication.run(IntegratorFileApplication.class, args);
    }

}
