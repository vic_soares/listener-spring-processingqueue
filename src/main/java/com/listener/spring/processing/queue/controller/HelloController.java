package com.listener.spring.processing.queue.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/upload")
@Tag(name = "Integrator", description = "Upload Files to Process")
public class HelloController {

    @GetMapping(path = "/hello")
    @Operation(summary = "Hello", description = "Hello World")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Success")
    })
    public Mono<String> helloWorld() {
        return Mono.just("Hello World!");
    }

}
